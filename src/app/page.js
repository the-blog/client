export default function Home() {
  return (
    <div>
      <HeroSection />
      <MostReadPosts />
    </div>
  );
}

export const HeroSection = () => {
  return (
    <div className="bg-black px-10 py-10">
      <div className="grid lg:grid-cols-2 grid-cols-1 lg:gap-10">
        <div>
          <div>
            <img
              width={1280}
              height={720}
              src={
                "https://media.licdn.com/dms/image/D5612AQGTNw-LzOi_2A/article-cover_image-shrink_720_1280/0/1701857084361?e=2147483647&v=beta&t=BiN0Af8nrSTXMp8VZZVEvorgFuohDre78arHdJcjeRY"
              }
            />
            <div className="mt-3">
              <p className="text-sm text-gray-400">April 8, 2024</p>
              <h2 className="mt-2 text-white font-semibold text-3xl">
                4 useState Mistakes You Should Avoid in React🚫
              </h2>
              <p className=" text-gray-400 mt-2">
                Introduction React.js has become a cornerstone of modern web
                development, with its unique approach to managing state within
                components. One common hook, useState, is fundamental but often
              </p>
            </div>
          </div>
        </div>
        <div>
          <div className="grid grid-cols-5 gap-5 mb-10">
            <img
              className="col-span-2"
              src={
                "https://media.licdn.com/dms/image/D5612AQGTNw-LzOi_2A/article-cover_image-shrink_720_1280/0/1701857084361?e=2147483647&v=beta&t=BiN0Af8nrSTXMp8VZZVEvorgFuohDre78arHdJcjeRY"
              }
            />
            <div className="col-span-3">
              <p className="text-sm text-gray-400">April 8, 2024</p>
              <h2 className="mt-2 text-white font-semibold text-xl">
                4 useState Mistakes You Should Avoid in React🚫
              </h2>
              <p className=" text-gray-400 mt-2">
                Introduction React.js has become a cornerstone of modern web
                development, with its unique approach to managing state within
                components. One common hook
              </p>
            </div>
          </div>

          <div className="grid grid-cols-5 gap-5 mb-10">
            <img
              className="col-span-2"
              src={
                "https://media.licdn.com/dms/image/D5612AQGTNw-LzOi_2A/article-cover_image-shrink_720_1280/0/1701857084361?e=2147483647&v=beta&t=BiN0Af8nrSTXMp8VZZVEvorgFuohDre78arHdJcjeRY"
              }
            />
            <div className="col-span-3">
              <p className="text-sm text-gray-400">April 8, 2024</p>
              <h2 className="mt-2 text-white font-semibold text-xl">
                4 useState Mistakes You Should Avoid in React🚫
              </h2>
              <p className=" text-gray-400 mt-2">
                Introduction React.js has become a cornerstone of modern web
                development, with its unique approach to managing state within
                components. One common hook
              </p>
            </div>
          </div>

          <div className="grid grid-cols-5 gap-5">
            <img
              className="col-span-2"
              src={
                "https://media.licdn.com/dms/image/D5612AQGTNw-LzOi_2A/article-cover_image-shrink_720_1280/0/1701857084361?e=2147483647&v=beta&t=BiN0Af8nrSTXMp8VZZVEvorgFuohDre78arHdJcjeRY"
              }
            />
            <div className="col-span-3">
              <p className="text-sm text-gray-400">April 8, 2024</p>
              <h2 className="mt-2 text-white font-semibold text-xl">
                4 useState Mistakes You Should Avoid in React🚫
              </h2>
              <p className=" text-gray-400 mt-2">
                Introduction React.js has become a cornerstone of modern web
                development, with its unique approach to managing state within
                components. One common hook
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export const MostReadPosts = () => {
  return (
    <div className="p-10">
      <p className="text-gray-400 text-2xl">Most Read Articles</p>
      <div className="mt-5 grid grid-cols-1 gap-8 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4">
        <BlogCardVert />
        <BlogCardVert />
        <BlogCardVert />
        <BlogCardVert />
        <BlogCardVert />
        <BlogCardVert />
        <BlogCardVert />
      </div>
    </div>
  );
};

export const BlogCardVert = () => {
  return (
    <div className="mb-5">
      <img
        width={1280}
        height={720}
        src={
          "https://media.licdn.com/dms/image/D5612AQGTNw-LzOi_2A/article-cover_image-shrink_720_1280/0/1701857084361?e=2147483647&v=beta&t=BiN0Af8nrSTXMp8VZZVEvorgFuohDre78arHdJcjeRY"
        }
      />
      <div className="mt-3">
        <p className="text-sm text-gray-400">April 8, 2024</p>
        <h2 className="mt-2 text-black font-semibold text-2xl">
          4 useState Mistakes You Should Avoid in React🚫
        </h2>
        <p className=" text-gray-400 mt-2">
          Introduction React.js has become a cornerstone of modern web
          development, with its unique approach to managing state
        </p>
      </div>
    </div>
  );
};
