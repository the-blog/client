import { BlogCardVert } from "@/app/page";
import BreadCrumb from "@/components/CustomUI/BreadCrumb";
import React from "react";

const Category = () => {
  return (
    <div className="p-10 pt-5">
      <BreadCrumb />
      <div className="lg:p-10">
        <p className="text-gray-400 text-2xl">Most Read Articles</p>
        <div className="mt-5 grid grid-cols-1 gap-8 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4">
          <BlogCardVert />
          <BlogCardVert />
          <BlogCardVert />
          <BlogCardVert />
          <BlogCardVert />
          <BlogCardVert />
          <BlogCardVert />
        </div>
      </div>
    </div>
  );
};

export default Category;
