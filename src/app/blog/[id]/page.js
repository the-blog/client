"use client";
import BreadCrumb from "@/components/CustomUI/BreadCrumb";
import dynamic from "next/dynamic";
import React from "react";
const BlockNoteEditor = dynamic(
  () => import("../../../components/BlockNoteEditor"),
  { ssr: false }
);

const Blog = () => {
  return (
    <div className="px-10 flex justify-center">
      <div className="max-w-[1100px] w-[100%]">
        <div className="py-5">
          <h3 className="text-4xl font-semibold mt-5">
            4 things to avoid while using useState() hook
          </h3>
          <p className="text-sm text-gray-400 my-2">Last Updated on April 8, 2024</p>
        </div>
        <div className="grid grid-cols-4">
          <div className="col-span-1 p-5 border-[1px] h-[600px] lg:sticky top-10">
            Table of Contents
          </div>
          <div className="col-span-3 h-[200vh]">
            <BlockNoteEditor />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Blog;
