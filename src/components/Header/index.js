"use client";
import Link from "next/link";
import { usePathname, useRouter } from "next/navigation";
import React from "react";
import Button from "../CustomUI/Button";

const Header = () => {
  const pathname = usePathname();
  const router = useRouter();

  const menuItems = [
    { name: "Tech", path: "/category/tech" },
    { name: "Science", path: "/category/science" },
    { name: "Programming", path: "/category/programming" },
  ];

  return (
    <div className="bg-black px-10">
      <div className="flex py-5 justify-between items-center">
        <div className="flex">
          {pathname !== "/" && (
            <div className="mr-10">
              <Link href="/">
                <p className="text-xl font-semibold text-[#76ABAE]">THE BLOG</p>
              </Link>
            </div>
          )}

          <div className="flex">
            {menuItems.map((item) => {
              if (item.path === pathname) {
                return (
                  <div key={item.path} className="mr-5">
                    <p className="text-white underline">{item.name}</p>
                  </div>
                );
              } else {
                return (
                  <div key={item.path} className="mr-5">
                    <Link href={item.path}>
                      <p className="text-white">{item.name}</p>
                    </Link>
                  </div>
                );
              }
            })}
          </div>
        </div>

        {/* <div>
          <Button action={() => {
            router.push('/auth/login')
          }}>
            Login
          </Button>
        </div> */}
      </div>

      {pathname === "/" ? (
        <div className="border-t-[1px] border-b-[1px] border-gray-700">
          <h1 className="text-[200px] text-center uppercase text-white font-semibold">
            The Blog
          </h1>
        </div>
      ) : (
        <></>
      )}
    </div>
  );
};

export default Header;
