'use client'
import Link from "next/link"
import { usePathname } from "next/navigation"
import React, { useEffect, useState } from 'react'

const BreadCrumb = () => {
  const pathname = usePathname()
  const [links, setLinks] = useState([{name: "Home", path: "/"}])

  useEffect(() => {
    const segments = pathname.split('/').filter(segment => segment.trim() !== '');
    
    if(segments[0] === "category") {
      setLinks([...links, { name: segments[1], path: `/category/${segments[1]}`}])
    } 
  }, [])

  return (
    <div className="flex">
      {links.map((link) => (
        <div className="flex" key={link.path}>
          {link.path !== "/" && <span className="mx-2">/</span>}
          <Link href={link.path}>
            <p>{link.name}</p>
          </Link>
        </div>
      ))}
    </div>
  )
}

export default BreadCrumb