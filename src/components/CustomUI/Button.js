import React from "react";

const Button = ({ children, action }) => {
  return (
    <div className="cursor-pointer border-[1px] text-white px-5 py-2 rounded border-gray-700" onClick={action}>
      {children}
    </div>
  );
};

export default Button;
