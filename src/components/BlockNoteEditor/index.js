"use client"; // this registers <Editor> as a Client Component
import React from "react";
import {
  BlockNoteView,
  useCreateBlockNote,
} from "@blocknote/react";

import "@blocknote/core/fonts/inter.css";
import "@blocknote/react/style.css";

export default function BlockNoteEditor() {
  const editor = useCreateBlockNote(
    {
      initialContent: JSON.parse(localStorage.getItem("blog")),
    },
    []
  );

  return (
    <BlockNoteView
      onChange={(e) => {
        localStorage.setItem("blog", JSON.stringify(e.document));
      }}
      editor={editor}
    />
  );
}
