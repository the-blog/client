import React from 'react'

const Footer = () => {
  return (
    <div className="bg-black px-10 py-2 text-right text-white">
      All copyright reserved under @Agile Coders Pvt. Ltd.
    </div>
  )
}

export default Footer